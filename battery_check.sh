#! /bin/bash
 # filename             
 # use                  
 # datetime             2022-01-27.23:05:57.250769643.05.4.027.+0800
 # access               public
 # category             ---- 类型 ▼ ----
 #                      
 # package              ---- 包 ▼ ----
 #                      
 # explain              ---- 功能说明 ▼ ----
 #                      
 # relate               ---- 相关联的模块 ▼ ----
 #                      
 # link                 ---- 链接地址 ▼ ----
 #                      http://timeline.menu
 #                      http://exploreroot.com
 #                      http://so-u.info
 # algorithm            ---- 算法 ▼ ----
 #                      
 # author               ---- 作者 ▼ ----
 #                      timeline.menu@gmail.com
 #                      timeline.menu@outlook.com
 # copyright            ---- 所有权 ▼ ----
 #                      timeline.menu has all rights reserved.
 # license              ---- 许可证信息 ▼ ----
 #                      TTL v1 (The Timeline.menu License, version 1.)
 # rely                 ---- 依赖于 ▼ ----
 #                      
 # rely_by              ---- 被依赖 ▼ ----
 #                      
 # version              ---- 版本号 ▼ ----
 #                      
 # changelog            ---- 修改记录 ▼ ----
 #                      2022-01-27.23:05:57.250769643.05.4.027.+0800
 #                       建立本文件
#

export DISPLAY=:0.0

capacity=$(cat /sys/class/power_supply/BAT0/capacity)
#echo $capacity
status=$(cat /sys/class/power_supply/BAT0/status)
#echo $status
basepath='/home/<username>/scripts/battery/audios/'

if [ "$status"x = "Discharging"x ]
then

#    echo $status
#    echo "电池放电中，将检查低电量状态。"
#    echo "While the battery is discharging, a low battery status will be checked. "
    echo $(date +'%Y-%m-%d.%H:%M:%S.%N')" discharging" >> ~/logs/battery/battery_status.$(date +'%Y-%m-%d') 2>&1 &
    if (( $capacity <= 5 ))
    then
#        mplayer $basepath'电量已低于20%，请充电.mp3'
#        mplayer $basepath'20.mp3'
#        mplayer $basepath'20.mp3' >> ~/logs/battery/battery_player.$(date +'%Y-%m-%d') 2>&1 &
#        mplayer -ao alsa $basepath'20.mp3' >> ~/logs/battery/battery_player.$(date +'%Y-%m-%d') 2>&1 &
#        eog $basepath'Untitled.png'
#        XDG_RUNTIME_DIR=/run/user/1000 mplayer $basepath'20.mp3' >> ~/logs/battery/battery_player.$(date +'%Y-%m-%d') 2>&1 &
#        XDG_RUNTIME_DIR=/run/user/1000 mplayer $basepath'电量已低于20%，请充电.mp3' >> ~/logs/battery/battery_player.$(date +'%Y-%m-%d') 2>&1 &
        XDG_RUNTIME_DIR=/run/user/1000 mplayer $basepath'电量已低于5%，请立即充电.mp3' >> ~/logs/battery/battery_player.$(date +'%Y-%m-%d') 2>&1 &
    elif (( $capacity <= 10 ))
    then
        XDG_RUNTIME_DIR=/run/user/1000 mplayer $basepath'电量已低于10%，请充电.mp3' >> ~/logs/battery/battery_player.$(date +'%Y-%m-%d') 2>&1 &
    elif (( $capacity <= 15 ))
    then
        XDG_RUNTIME_DIR=/run/user/1000 mplayer $basepath'电量已低于15%，请立即充电.mp3' >> ~/logs/battery/battery_player.$(date +'%Y-%m-%d') 2>&1 &
    elif (( $capacity <= 20 ))
    then
        XDG_RUNTIME_DIR=/run/user/1000 mplayer $basepath'电量已低于20%，请充电.mp3' >> ~/logs/battery/battery_player.$(date +'%Y-%m-%d') 2>&1 &
    fi
           
elif [ "$status"x = "Charging"x ]
then

#    echo $status
#    echo "电池充电中，将检查高电量状态。"
#    echo "While the battery is charging, the high battery status will be checked. "
    echo $(date +'%Y-%m-%d.%H:%M:%S.%N')" charging" >> ~/logs/battery/battery_status.$(date +'%Y-%m-%d') 2>&1 &
    if (( $capacity >= 100 ))
    then
#        mplayer $basepath'电量已低于20%，请充电.mp3'
#        mplayer $basepath'20.mp3' >> ~/logs/battery/battery_player.$(date +'%Y-%m-%d') 2>&1 &
#        XDG_RUNTIME_DIR=/run/user/1000 mplayer $basepath'电量已达到80%，请断开充电器.mp3' >> ~/logs/battery/battery_player.$(date +'%Y-%m-%d') 2>&1 &
#        XDG_RUNTIME_DIR=/run/user/1000 mplayer $basepath'电量已达到90%，请断开充电器.mp3' >> ~/logs/battery/battery_player.$(date +'%Y-%m-%d') 2>&1 &
#        XDG_RUNTIME_DIR=/run/user/1000 mplayer $basepath'电量已达到95%，请断开充电器.mp3' >> ~/logs/battery/battery_player.$(date +'%Y-%m-%d') 2>&1 &
#        XDG_RUNTIME_DIR=/run/user/1000 mplayer $basepath'电量已达到100%，请断开充电器.mp3' >> ~/logs/battery/battery_player.$(date +'%Y-%m-%d') 2>&1 &
        XDG_RUNTIME_DIR=/run/user/1000 mplayer $basepath'电量已达到100%，请断开充电器.mp3' >> ~/logs/battery/battery_player.$(date +'%Y-%m-%d') 2>&1 &
    elif (( $capacity >= 95 ))
    then
        XDG_RUNTIME_DIR=/run/user/1000 mplayer $basepath'电量已达到95%，请断开充电器.mp3' >> ~/logs/battery/battery_player.$(date +'%Y-%m-%d') 2>&1 &
    elif (( $capacity >= 90 ))
    then
        XDG_RUNTIME_DIR=/run/user/1000 mplayer $basepath'电量已达到90%，请断开充电器.mp3' >> ~/logs/battery/battery_player.$(date +'%Y-%m-%d') 2>&1 &
    elif (( $capacity >= 85 ))
    then
        XDG_RUNTIME_DIR=/run/user/1000 mplayer $basepath'电量已达到85%，请断开充电器.mp3' >> ~/logs/battery/battery_player.$(date +'%Y-%m-%d') 2>&1 &
    elif (( $capacity >= 80 ))
    then
        XDG_RUNTIME_DIR=/run/user/1000 mplayer $basepath'电量已达到80%，请断开充电器.mp3' >> ~/logs/battery/battery_player.$(date +'%Y-%m-%d') 2>&1 &
    fi

else
    echo $(date +'%Y-%m-%d.%H:%M:%S.%N')" 无法检测电池状态" >> ~/logs/battery/battery_status.$(date +'%Y-%m-%d') 2>&1 &
    echo $(date +'%Y-%m-%d.%H:%M:%S.%N')" Unable to detect battery status " >> ~/logs/battery/battery_status.$(date +'%Y-%m-%d') 2>&1 &
fi


